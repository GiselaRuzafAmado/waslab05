package twitter;

import java.util.Date;

import twitter4j.FilterQuery;
import twitter4j.ResponseList;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.StatusListener;

public class SimpleClient {

	public static void main(String[] args) throws Exception {
		
		final Twitter twitter = new TwitterFactory().getInstance();
		
		Date now = new Date();
		//String latestStatus = "Hey @fib_was, we've just completed task #4 [timestamp: "+now+"]";
		//Status status = twitter.updateStatus(latestStatus);
		//System.out.println("Successfully updated the status to: " + status.getText());
		/*Status lastTweet = twitter.getUserTimeline().get(0);
		System.out.println(lastTweet.getText());
		twitter.retweetStatus(lastTweet.getId());*/
		
		final TwitterStream stream = new TwitterStreamFactory().getInstance();
		stream.addListener(new StatusListener() {
		      public void onStatus(Status newTweet) {
		          System.out.println(newTweet.getUser().getName() + "(@" + newTweet.getUser().getScreenName() + "): " + newTweet.getText() + "\n");
		      }

			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				// TODO Auto-generated method stub
				
			}
			});
		   FilterQuery tweetFilterQuery = new FilterQuery();
		   tweetFilterQuery.track(new String[]{"#barcelona"});
		   stream.filter(tweetFilterQuery);
	}
}
